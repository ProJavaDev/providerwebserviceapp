package com.provider.uws.service.Impl;

import com.provider.uws.cons.Constants;
import com.provider.uws.cons.Status;
import com.provider.uws.db.Client;
import com.provider.uws.db.ProviderRepository;
import com.provider.uws.db.Transaction;
import com.provider.uws.db.Wallet;
import com.provider.uws.generated.*;
import com.provider.uws.service.ProviderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class ProviderServiceImpl implements ProviderService {

    private final ProviderRepository providerRepository;
    private SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Override
    public GetInformationResult getInformation(GetInformationArguments value) {
        GetInformationResult result = new GetInformationResult();
        result.setTimeStamp(getNowTimeStamp());
        List<GenericParam> resParam = new ArrayList<>();
        Long clientId = null;

        for (GenericParam param : value.getParameters()) {
            //validation phone number
            if (param.getParamKey().equals("phone_number")) {
                if (!phoneNumberIsValid(param.getParamValue())) {
                    result.setStatus(1);
                    result.setErrorMsg("Phone number is invalid!");
                    resParam.add(param);
                    result.setParameters(resParam);
                    return result;
                }
            }
            //validation card number
            if (param.getParamKey().equals("card_number")) {
                if (!cardNumberIsValid(param.getParamValue())) {
                    result.setStatus(1);
                    result.setErrorMsg("Card number is invalid!");
                    resParam.add(param);
                    result.setParameters(resParam);
                    return result;
                }
            }
            //get clientId
            if (param.getParamKey().equals("client_id")) {
                clientId = Long.valueOf(param.getParamValue());
            }
        }

        if (clientId != null) {
            Client client = providerRepository.findClientById(clientId);
            Wallet clientWallet = providerRepository.findWalletById(client.getWalletId());
            resParam.add(builderParam("name", client.getName()));
            resParam.add(builderParam("balance", String.valueOf(clientWallet.getBalance())));
            resParam.add(builderParam("card_number", clientWallet.getCardNumber()));
            resParam.add(builderParam("your_wallet_replenishment_limit", String.valueOf(Constants.WALLET_REPLENISHMENT_LIMIT - clientWallet.getBalance())));
        }

        result.setParameters(resParam);
        return result;
    }

    @Override
    public PerformTransactionResult performTransaction(PerformTransactionArguments value) {
        PerformTransactionResult result = new PerformTransactionResult();
        result.setTimeStamp(getNowTimeStamp());
        List<GenericParam> resParam = new ArrayList<>();
        String cardNumber = null;

        for (GenericParam param : value.getParameters()) {
            //validation phone number
            if (param.getParamKey().equals("phone_number")) {
                if (!phoneNumberIsValid(param.getParamValue())) {
                    result.setStatus(1);
                    result.setErrorMsg("Phone number is invalid!");
                    resParam.add(param);
                    result.setParameters(resParam);
                    return result;
                }
            }
            //validation card number
            if (param.getParamKey().equals("card_number")) {
                cardNumber = param.getParamValue();
                if (!cardNumberIsValid(cardNumber)) {
                    result.setStatus(1);
                    result.setErrorMsg("Card number is invalid!");
                    resParam.add(param);
                    result.setParameters(resParam);
                    return result;
                }
            }
        }

        if (value.getAmount() <= 0L) {
            result.setStatus(1);
            result.setErrorMsg("'amount' must be greater than zero!");
            result.setParameters(resParam);
            return result;
        }

        Wallet wallet = providerRepository.findWalletByCardNumber(cardNumber);

        if (wallet == null) {
            result.setStatus(1);
            result.setErrorMsg("Wallet not fount by card_number:" + cardNumber);
            resParam.add(builderParam("card_number", cardNumber));
            result.setParameters(resParam);
            return result;
        }

        if (value.getAmount() > (Constants.WALLET_REPLENISHMENT_LIMIT - wallet.getBalance())) {
            result.setStatus(1);
            result.setErrorMsg("The amount is greater than the limit! your limit:" +
                    (Constants.WALLET_REPLENISHMENT_LIMIT - wallet.getBalance()));
            resParam.add(builderParam("amount", String.valueOf(value.getAmount())));
            result.setParameters(resParam);
            return result;
        }

        if (providerRepository.findTransactionById(value.getTransactionId()) != null) {
            result.setStatus(1);
            result.setErrorMsg("Send another transactionId on which the transaction was created by this transactionId");
            resParam.add(builderParam("transactionId", String.valueOf(value.getTransactionId())));
            result.setParameters(resParam);
            return result;
        }

        //create transaction
        providerRepository.saveTransaction(Transaction.builder()
                .id(value.getTransactionId())
                .amount(value.getAmount())
                .date(timeFormat.format(new Date()))
                .walletId(wallet.getId())
                .clientId(providerRepository.findClientByWalletId(wallet.getId()).getId())
                .status(Status.SUCCESS)
                .build());
        wallet.setBalance(wallet.getBalance() + value.getAmount());
        providerRepository.updateWalletById(wallet.getId(), wallet);
        result.setStatus(0);
        result.setErrorMsg("OK");
        resParam.add(builderParam("balance", String.valueOf(wallet.getBalance())));
        resParam.add(builderParam("date", timeFormat.format(new Date())));
        result.setParameters(resParam);
        result.setProviderTrnId(wallet.getId());
        return result;
    }


    private GenericParam builderParam(String key, String value) {
        GenericParam genericParam = new GenericParam();
        genericParam.setParamKey(key);
        genericParam.setParamValue(value);
        return genericParam;
    }

    private boolean phoneNumberIsValid(String phoneNumber) {
        Pattern pattern = Pattern.compile("^(\\+\\d{1,3}( )?)?((\\(\\d{1,3}\\))|\\d{1,3})[- .]?\\d{3,4}[- .]?\\d{4}$");
        return pattern.matcher(phoneNumber).matches();
    }

    private XMLGregorianCalendar getNowTimeStamp() {
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());
        } catch (DatatypeConfigurationException e) {
            return null;
        }
    }

    private boolean cardNumberIsValid(String cardNum) {
        return checkingLuhnAlgorithm(cardNum);
    }

    private boolean checkingLuhnAlgorithm(String cardNum) {
        int nDigits = cardNum.length();
        int nSum = 0;
        boolean isSecond = false;
        for (int i = nDigits - 1; i >= 0; i--) {
            int d = cardNum.charAt(i) - '0';
            if (isSecond == true)
                d = d * 2;
            nSum += d / 10;
            nSum += d % 10;
            isSecond = !isSecond;
        }
        return (nSum % 10 == 0);
    }
}
