package com.provider.uws.service;

import com.provider.uws.generated.GetInformationArguments;
import com.provider.uws.generated.GetInformationResult;
import com.provider.uws.generated.PerformTransactionArguments;
import com.provider.uws.generated.PerformTransactionResult;

public interface ProviderService {

    GetInformationResult getInformation(GetInformationArguments value);

    PerformTransactionResult performTransaction(PerformTransactionArguments value);

}
