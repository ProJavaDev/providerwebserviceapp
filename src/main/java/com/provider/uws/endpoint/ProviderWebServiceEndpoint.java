package com.provider.uws.endpoint;

import com.provider.uws.generated.*;
import com.provider.uws.service.ProviderService;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.bind.JAXBElement;

@Endpoint
@RequiredArgsConstructor
public class ProviderWebServiceEndpoint {

    private final ProviderService providerService;
    public static final String NAMESPACE_URI = "http://uws.provider.com/";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "PerformTransactionArguments")
    @ResponsePayload
    public JAXBElement<PerformTransactionResult> performTransaction(@RequestPayload JAXBElement<PerformTransactionArguments> arguments) {
        return ObjectFactory.createPerformTransactionResult(providerService.performTransaction(arguments.getValue()));
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetInformationArguments")
    @ResponsePayload
    public JAXBElement<GetInformationResult> getInformation(@RequestPayload JAXBElement<GetInformationArguments> arguments) {
        return ObjectFactory.createGetInformationResult(providerService.getInformation(arguments.getValue()));
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetStatementArguments")
    @ResponsePayload
    public JAXBElement<GetStatementResult> getStatement(@RequestPayload JAXBElement<GetStatementArguments> arguments) {
        return ObjectFactory.createGetStatementResult(new GetStatementResult());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CheckTransactionArguments")
    @ResponsePayload
    public JAXBElement<CheckTransactionResult> checkTransaction(@RequestPayload JAXBElement<CheckTransactionArguments> arguments) {
        return ObjectFactory.createCheckTransactionResult(new CheckTransactionResult());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ChangePasswordArguments")
    @ResponsePayload
    public JAXBElement<ChangePasswordResult> changePassword(@RequestPayload JAXBElement<ChangePasswordArguments> arguments) {
        return ObjectFactory.createChangePasswordResult(new ChangePasswordResult());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CancelTransactionArguments")
    @ResponsePayload
    public JAXBElement<CancelTransactionResult> cancelTransaction(@RequestPayload JAXBElement<CancelTransactionArguments> arguments) {
        return ObjectFactory.createCancelTransactionResult(new CancelTransactionResult());
    }

}
