package com.provider.uws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProviderWebServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderWebServiceApplication.class, args);
    }
}
