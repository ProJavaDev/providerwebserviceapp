package com.provider.uws.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Client {
    private Long id;
    private String username;
    private String password;
    private String name;
    private Long walletId;
    private String phoneNumber;
}
