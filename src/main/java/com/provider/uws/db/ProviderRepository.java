package com.provider.uws.db;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ProviderRepository {

    private static final Map<Long, Client> clients = new HashMap<>();
    private static final Map<Long, Wallet> wallets = new HashMap<>();
    private static final Map<Long, Transaction> transactions = new HashMap<>();

    @PostConstruct
    public void initData() {
        Client pushkin = Client.builder()
                .id(634247L)
                .username("user")
                .password("pwd")
                .name("Пушкин А.С.")
                .walletId(3625L)
                .phoneNumber("+998996665533")
                .build();
        clients.put(pushkin.getId(), pushkin);

        Wallet pushkinWallet = Wallet.builder()
                .id(3625L)
                .cardNumber("8600060980206503")
                .balance(420000L)
                .clientId(634247L)
                .build();
        wallets.put(pushkinWallet.getId(), pushkinWallet);
    }

    public Client findClientById(Long clientId) {
        return clients.get(clientId);
    }

    public Client findClientByWalletId(Long walletId) {
        List<Client> filterClientList = clients.values().stream().filter(client -> client.getWalletId().equals(walletId)).collect(Collectors.toList());
        if (filterClientList.isEmpty()) {
            return null;
        }
        return filterClientList.get(0);
    }

    public Wallet findWalletById(Long id) {
        return wallets.get(id);
    }

    public Wallet findWalletByCardNumber(String cardNumber) {
        List<Wallet> filterWalletList = wallets.values().stream().filter(wallet -> wallet.getCardNumber().equals(cardNumber)).collect(Collectors.toList());
        if (filterWalletList.isEmpty()) {
            return null;
        }
        return filterWalletList.get(0);
    }

    public Wallet updateWalletById(Long id, Wallet wallet) {
        return wallets.replace(id, wallet);
    }

    public Transaction findTransactionById(Long id) {
        return transactions.get(id);
    }

    public Transaction saveTransaction(Transaction transaction) {
        transactions.put(transaction.getId(), transaction);
        return transaction;
    }

}
