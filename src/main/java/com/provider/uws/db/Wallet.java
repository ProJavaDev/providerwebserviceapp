package com.provider.uws.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Wallet {
    private Long id;
    private Long balance;
    private String cardNumber;
    private Long clientId;
}
