package com.provider.uws.db;

import com.provider.uws.cons.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {
    private Long id;
    private Long amount;
    private String date;
    private Long clientId;
    private Long walletId;
    private Status status;
}
